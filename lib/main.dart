import 'package:flutter/material.dart';
import './chart.dart';
import './add_transaction.dart';
import './transactions_list.dart';

void main() {
  runApp(MyApp());
}

class Transaction {
  String id;
  String name;
  double amount;
  DateTime dateTime;

  Transaction({this.id, this.name, this.amount, this.dateTime});
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Transaction> transactions = [
    Transaction(
        id: 't1',
        name: 'transaction one hahahahhahahaha',
        amount: 10.5,
        dateTime: DateTime.now()),
    Transaction(
        id: 't2',
        name: 'transaction two',
        amount: 11.5,
        dateTime: DateTime.now()),
    Transaction(
        id: 't3',
        name: 'transaction three',
        amount: 12.5,
        dateTime: DateTime.now()),
  ];

  void _addTransaction(String title, double amount) {
    final transaction = Transaction(
        id: DateTime.now().toString(),
        name: title,
        amount: amount,
        dateTime: DateTime.now());

    setState(() {
      transactions.add(transaction);
    });

    Navigator.of(context).pop();
  }

  void openSheet(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (_) {
          return AddTransaction(_addTransaction);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('sasa app'),
        actions: [
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                openSheet(context);
              }),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Chart(transactions),
            TransactionsList(transactions),
          ],
        ),
      )
      // Center is a layout widget. It takes a single child and positions it
      // in the middle of the parent.

      ,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          openSheet(context);
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
