import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'main.dart';

class Chart extends StatelessWidget {
  List<Transaction> transactions;

  Chart(this.transactions);

  List<Map<String, Object>> get generateNewItems {
    return List.generate(7, (index) {
      DateTime day = DateTime.now().subtract(Duration(days: index));
      double amount = 0.0;

      for (var tx in transactions) {
        if (tx.dateTime.day == day.day &&
            tx.dateTime.month == day.month &&
            tx.dateTime.year == day.year) {
          amount += tx.amount;
        }
      }

      return {'day': DateFormat.E().format(day), 'amount': amount};
    });
  }

  @override
  Widget build(BuildContext context) {
    print(generateNewItems);
    return Container();
  }
}
